# Lab ADC 500 Repo

## Before building
This lab requieres two external repositories, one is the Comblock and the other is the ADC500 driver. You can clone both manually, however both are added in this project as submodules. To initialize them you can use the following commands:

```
git clone https://gitlab.com/ictp-mlab/lab_adc500.git --recursive
```


## Autobuild

### From linux Terminal

Setup Vivado enviroment. 
```
$ source <path_to_vivado>/Vivado/2022.1/settings64.sh
$ vivado -s lab_adc500.tcl
```

### From Vivado Terminal
1. From **Tcl Console** change directory to ```<path_to_lab_adc500_git>``` and run:
2. 
   ```
   cd <path_to_lab_adc500_git>
   source lab_adc500.tcl
   ```

#### From Sources 
1. Create a new vivado project for Zedboard 
3. Add *ip_repo* repository to the IP location.
4. From "Add sources/Add or create design sources" menu, add  *ip_repo/adc500* directory to the sources  
5. From "Add sources/Add or create constraints" menu, add "const/Zedboard" directory
6. Change directory to the git repository in the TCL folder
7. 
   ```
   cd <path_to_lab_adc500_git>
   ```
8. Generate the block diagram by running the following command:
   ```
   source bd/Top.tcl
   ```
9. Create wrapper and set it as top. 
10. Generate bitstream.

### More information
[smr3765 Lab ADC500](https://gitlab.com/ictp-mlab/smr3765/-/wikis/Project/lab_adc500)